/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author TUFGaming
 */
import java.util.*;

public class XO {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);

        //Start//
        System.out.println("Welcom to OX Game.");
        String winner = "-";
        int row = 0;
        int col = 0;
        int round = 0;

        //create field//
        String field[][]
                = {{"-", "-", "-"},
                {"-", "-", "-"},
                {"-", "-", "-"}};

        try {
            while (true) {
                //field//
                System.out.println(" 1 2 3");
                int num = 1;
                for (int i = 0; i < 3; i++) {
                    System.out.print(num + " ");
                    for (int k = 0; k < 3; k++) {
                        System.out.print(field[i][k] + " ");
                    }
                    System.out.println();
                    num++;
                }
                //Input X//

                System.out.println("X turn.");
                System.out.println("Please input Row Col: ");
                boolean incurrect = true;
                while (incurrect == true) {
                    row = kb.nextInt() - 1;
                    col = kb.nextInt() - 1;
                    if (field[row][col].equals("-")) {
                        field[row][col] = "X";
                        incurrect = false;
                    } else {
                        System.out.println("ERROR. You need to enter X again.");
                        incurrect = true;
                    }
                }
                
                //X condition//
                for (int i = 0; i < 3; i++) {
                    if (field[i][0].equals("X") && field[i][1].equals("X") && field[i][2].equals("X")) {
                        winner = "X";
                        break;
                    }
                    if (field[0][i].equals("X") && field[1][i].equals("X") && field[2][i].equals("X")) {
                        winner = "X";
                        break;
                    }
                }
                if (winner.equals("X")) {
                    break;
                }
                if (((field[0][0].equals("X") && field[1][1].equals("X") && field[2][2].equals("X"))
                        || (field[0][2].equals("X") && field[1][1].equals("X") && field[2][0].equals("X")))) {

                    winner = "X";
                    break;
                }
                round++;
                //Draw condition//
                if (round >= 9) {
                    System.out.println("Draw.");
                    break;
                }
                //field//
                System.out.println(" 1 2 3");
                num = 1;
                for (int i = 0; i < 3; i++) {
                    System.out.print(num + " ");
                    for (int k = 0; k < 3; k++) {
                        System.out.print(field[i][k] + " ");
                    }
                    System.out.println();
                    num++;
                }
                //Input O//
                System.out.println("O turn.");
                System.out.println("Please input Row Col: ");
                boolean incurrect2 = true;
                while (incurrect2 == true) {
                    row = kb.nextInt() - 1;
                    col = kb.nextInt() - 1;
                    if (field[row][col].equals("-")) {
                        field[row][col] = "O";
                        incurrect2 = false;
                    } else {
                        System.out.println("ERROR. You need to enter O again.");
                        incurrect2 = true;
                    }
                }

                //O condition//
                for (int i = 0; i < 3; i++) {
                    if (field[i][0].equals("O") && field[i][1].equals("O") && field[i][2].equals("O")) {
                        winner = "O";
                        break;
                    }
                    if (field[0][i].equals("O") && field[1][i].equals("O") && field[2][i].equals("O")) {
                        winner = "O";
                        break;
                    }
                }
                if (winner.equals("O")) {
                    break;
                }
                if (((field[0][0].equals("O") && field[1][1].equals("O") && field[2][2].equals("O"))
                        || (field[0][2].equals("O") && field[1][1].equals("O") && field[2][0].equals("O")))) {

                    winner = "O";
                    break;
                }
                round++;
            }
            //catch//
        } catch (ArrayIndexOutOfBoundsException E) {
            System.out.println("Error please use input that less than 3.");

        }
        //field//
        System.out.println(" 1 2 3");
        int num = 1;
        for (int i = 0; i < 3; i++) {
            System.out.print(num + " ");
            for (int k = 0; k < 3; k++) {
                System.out.print(field[i][k] + " ");
            }
            System.out.println();
            num++;
        }
        //Print//
        if (winner.equals("X")) {
            System.out.println("Player X Win.");
        } else if (winner.equals("O")) {
            System.out.println("Player O Win.");
        }
        System.out.println("Bye bye.");
    }
}
